import {CalendarEvent} from './calendar-event';

export class CalendarNextEvent {

  constructor(public next: string, public event: CalendarEvent ){}
}
