import {Component, OnInit, OnDestroy} from "@angular/core";
import { CalendarEventsService } from '../calendar-events.service';

@Component({
  selector: 'app-upcoming-events',
  templateUrl: 'upcoming-events.component.html',
  styleUrls: ['upcoming-events.component.scss']
})
export class UpcomingEventsComponent implements OnInit, OnDestroy {

  constructor(public calendarEventsService: CalendarEventsService) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.calendarEventsService.endTimers();
  }

}
