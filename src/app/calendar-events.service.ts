import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GoogleCalendarEvents } from './google-calendar-events';
import { SimpleTimer} from 'ng2-simple-timer';
import { CalendarNextEvent } from './calendar-next-event';
import { CalendarEvent } from './calendar-event';
import { DaysCalendarEvents } from './days-calendar-events';
import { forkJoin } from 'rxjs/observable/forkJoin';

@Injectable()
export class CalendarEventsService {

  public upcomingEvents: Array<DaysCalendarEvents> = [];
  public nextEvents: Array<CalendarNextEvent> = [];
  timer1minId: string;
  public googleCalendarAPIKey: string = null;
  public googleCalendarIds: Array<string> = [];

  constructor(private http: HttpClient, private timer: SimpleTimer) {
    // get events on startup
    this.http.get('./assets/calendar-configuration.json').subscribe( (response) => {
      this.googleCalendarAPIKey = response['googleCalendarAPIKey'];
      this.googleCalendarIds = response['googleCalendarIds'];
      this.getUpcomingEvents();
      this.getNextEvents();
    });

    // get events every 15 minutes
    this.timer.newTimer( '1min', 60);
    this.timer1minId = this.timer.subscribe(
      '1min',
      () => {
        let minutes: string = new Date(Date.now()).toTimeString().substr(3,2);
        if (minutes === '00' || minutes === '15' || minutes === '30' || minutes === '45') {
          this.getUpcomingEvents();
          this.getNextEvents();
        }
      });
  }

  // get the next two weeks worth of events and group them by date
  getUpcomingEvents() {
    let now = Date.now();
    let upcoming: Array<CalendarEvent> = [];
    let promises = [];
    for (let i = 0; i < this.googleCalendarIds.length; i++) {
      promises[i] =
        this.http.get<GoogleCalendarEvents>('https://www.googleapis.com/calendar/v3/calendars/' +
          this.googleCalendarIds[i] +
          '/events?orderBy=startTime&singleEvents=true' +
          '&timeMin=' + new Date(now).toISOString() +
          '&timeMax=' + new Date(now + 1.21e+9/2).toISOString() + // 1.21e+9 is two weeks in ms - now it is cut down to 1 week.
          '&fields=items(end%2FdateTime%2Clocation%2Cstart%2FdateTime%2Csummary%2Cdescription)' +
          '&key=' + this.googleCalendarAPIKey
        );
    }

    forkJoin(promises).subscribe( (values : Array<GoogleCalendarEvents>) => {
      for (let i = 0; i < values.length; i++ ) {
        for (let j = 0; j < values[i].items.length; j++) {
          upcoming.push(values[i].items[j]);
        }
      }
  console.log(upcoming)
      upcoming.sort( function(event1 : CalendarEvent, event2 : CalendarEvent) {
        if ( event1.start.dateTime < event2.start.dateTime ){
          return -1;
        } else if ( event1.start.dateTime > event2.start.dateTime ){
          return 1;
        }else{
          return 0;
        }
      });

      let events: Array<DaysCalendarEvents> = [];

      if (upcoming.length > 0) {
        let currentDate = upcoming[0].start.dateTime.substr(0, 10);
        let daysCalendarEvents: DaysCalendarEvents = new DaysCalendarEvents(currentDate, []);
        daysCalendarEvents.events.push(upcoming[0]);

        for (let i = 1; i < upcoming.length; i++) {
          let event: CalendarEvent = upcoming[i];

          if (event.start.dateTime.substr(0, 10) !== currentDate) {
            events.push(daysCalendarEvents);
            currentDate = upcoming[i].start.dateTime.substr(0, 10);
            daysCalendarEvents = new DaysCalendarEvents(currentDate, []);
          }
          daysCalendarEvents.events.push(event);
        }
        events.push(daysCalendarEvents);
      } else {
        events = null;
      }

      this.upcomingEvents = events;
    });
  }

  // need to change to get only PAL session calendar. For now I just set the length of the array of calendar IDs to 1.
  // get the next two hours worth of events and label them as currently happening, or coming up
  getNextEvents() {
    let now = Date.now();
    let upcoming: Array<CalendarEvent> = [];
    let promises = [];
    for (let i = 0; i < 1; i++) { //this.googleCalendarIds.length
      promises[i] =
        this.http.get<GoogleCalendarEvents>('https://www.googleapis.com/calendar/v3/calendars/' +
          this.googleCalendarIds[i] +
          '/events?orderBy=startTime&singleEvents=true' +
          '&timeMin=' + new Date(now).toISOString() +
          '&timeMax=' + new Date(now + 7200000).toISOString() + // 1.21e+9 is two weeks in ms
          '&fields=items(end%2FdateTime%2Clocation%2Cstart%2FdateTime%2Csummary%2Cdescription)' +
          '&key=' + this.googleCalendarAPIKey
        );
    }

    forkJoin(promises).subscribe( (values : Array<GoogleCalendarEvents>) => {
      for (let i = 0; i < values.length; i++ ) {
        for (let j = 0; j < values[i].items.length; j++) {
          upcoming.push(values[i].items[j]);
        }
      }

      upcoming.sort( function(event1 : CalendarEvent, event2 : CalendarEvent) {
        if ( event1.start.dateTime < event2.start.dateTime ){
          return -1;
        } else if ( event1.start.dateTime > event2.start.dateTime ){
          return 1;
        }else{
          return 0;
        }
      });

      let events: Array<CalendarNextEvent> = [];
      for (let i = 0; i < upcoming.length; i++) {
        let event: CalendarEvent = upcoming[i];
        if (new Date(event.end.dateTime) <= new Date(now)) {
          // past end time, don't display
        }
        if (new Date(event.start.dateTime) <= new Date(now) && new Date(event.end.dateTime) > new Date(now)) {
          events.push(new CalendarNextEvent("NOW IN SESSION", event));
        } else {
          events.push(new CalendarNextEvent("COMING UP", event));
        }
      }
      this.nextEvents = events;
    });
  }

  endTimers() {
    this.timer.delTimer('1min');
  }
}
